using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverridenB : Main
{
    protected override int AbstractA(string s)
    {
        char[] glasnie = new char[6] { 'a', 'e', 'i', 'o', 'u', 'y' };

        int count = 0;

        for (int i = 0; i < s.Length; i++)
        {
            for (int j = 0; j < glasnie.Length; j++)
            {
                if(glasnie[j] == s[i])
                {
                    count++;
                }
            }
        }

        return count;
    }

    protected override int AbstractB()
    {
        return Random.Range(1, 3);
    }

    protected override int VirtualA(int a,int b)
    {
        return a * b;
    }

    protected override int VirtualB(int c)
    {
        return c % 7;
    }
}
