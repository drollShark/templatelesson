using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverridenA : Main
{
    protected override int AbstractA(string s)
    {
        return s.Length;
    }

    protected override int AbstractB()
    {
        return 42;
    }
}
