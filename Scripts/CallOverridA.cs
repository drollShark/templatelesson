using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallOverridA : MonoBehaviour
{
    private void Start()
    {
        OverridenA overridenA = new OverridenA();
        Debug.Log(overridenA.TemplateMethods("����", 1, 2, 3));
    }
}
