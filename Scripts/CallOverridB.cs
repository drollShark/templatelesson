using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallOverridB : MonoBehaviour
{
    private void Start()
    {
        OverridenB overridenB = new OverridenB();

        Debug.Log(overridenB.TemplateMethods("Morning", 1, 2, 3));
    }
}
