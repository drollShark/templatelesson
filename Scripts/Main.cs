using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Main : MonoBehaviour
{
    protected abstract int AbstractA (string s);

    protected abstract int AbstractB();

    protected virtual int VirtualA(int a, int b)
    {
        return a + b;
    }

    protected virtual int VirtualB(int c)
    {
        return (int)Mathf.Pow(c,2);
    }

    public int TemplateMethods(string s, int a, int b, int c)
    {
        return (int)Mathf.Pow(AbstractA(s), AbstractB() + VirtualA(a, b) * VirtualB(c));
    }
}
